Magento-MissingPaymentMethod
============================

This is a module for Magento giving the possibility to handle missing payment 
methods in adminhtml order details.

The method 'missing' will be used for those orders.

