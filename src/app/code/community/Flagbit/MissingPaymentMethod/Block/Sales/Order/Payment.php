<?php
/**
 * Adminhtml sales order payment information
 *
 * @package     Flagbit_MissingPaymentMethod
 * @author      Uwe Kleinmann <uwe@codekunst.com>
 */
class Flagbit_MissingPaymentMethod_Block_Sales_Order_Payment extends Mage_Adminhtml_Block_Sales_Order_Payment
{
    public function setPayment($payment)
    {
        if(!Mage::helper('payment')->getMethodInstance($payment->getMethod())) {
            // Set missing payment method
            $method = Mage::getModel('flagbit_missingpaymentmethod/method_missing');
            $payment->setMethodInstance($method);
        }
        return parent::setPayment($payment);
    }
}
