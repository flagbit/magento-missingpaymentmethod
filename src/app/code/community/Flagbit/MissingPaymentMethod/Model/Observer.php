<?php
/**
 * Observer.php
 *
 * @category   Flagbit
 * @package
 * @copyright  Copyright (c) 02.10.2014 Flagbit GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Flagbit_MissingPaymentMethod_Model_Observer {

    /**
     * @param Varien_Event_Observer $observer
     */
    public function setMissingPaymentMethod(Varien_Event_Observer $observer)
    {
        if(Mage::getDesign()->getArea() == 'adminhtml') {
            /** @var Mage_Sales_Model_Order $order */
            $order = $observer->getEvent()->getOrder();

            try{
                // if we dont have one method we assign the missing method
                // just to view order on admin backend
                $instance = $order->getPayment()->getMethodInstance();
            } catch (Mage_Core_Exception $e) {
                $instance = Mage::helper('payment')->getMethodInstance('flagbit_missingpaymentmethod');
                $instance->setInfoInstance($order->getPayment());
                $order->getPayment()->setMethodInstance($instance);
            }
        }
    }
}
